﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T1_EXAMEN.Models;

namespace T1_EXAMEN.Controllers
{
    public class PostController : Controller
    {
        private readonly T1_EXAMEN_CONTEXT context;

        public PostController(T1_EXAMEN_CONTEXT context)
        {
            this.context = context;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var post = context.Posts.OrderByDescending(o => o.FechaCreacion).ToList();
            return View(post);
        }

        [HttpPost]
        public ActionResult Registrar(Post post)
        {
            post.FechaCreacion = DateTime.Now;
            context.Posts.Add(post);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Registrar()
        {
            return View("Registrar", new Post());
        }
        [HttpGet]
        public ActionResult Detalle(int id)
        {
            var post = context.Posts.Where(o => o.Id == id).FirstOrDefault();
            ViewBag.Comentario = context.Comentarios.
                Where(o => o.IdPost == id).
                OrderByDescending(o => o.FechaComentario).
                ToList();
            return View(post);
        }
        [HttpPost]
        public ActionResult RegistrarComentario(Comentario comentario)
        {
            comentario.FechaComentario = DateTime.Now;
            context.Add(comentario);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult RegistrarComentario(int id)
        {
            ViewBag.Id = id;
            return View("RegistrarComentario", new Comentario());
        }
    }
}
