﻿using System;
using System.Collections.Generic;

namespace T1_EXAMEN.Models
{
    public class Post
    {
        public int Id { get; set; }
        public String Nombre { get; set; }
        public String Autor { get; set; }
        public String Contenido { get; set; }
        public DateTime FechaCreacion { get; set; }
        public List<Comentario> Comentario { get; set; }
    }
}
