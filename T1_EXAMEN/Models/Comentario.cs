﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace T1_EXAMEN.Models
{
    public class Comentario
    {
        public int Id { get; set; }
        public String Opinion { get; set; }
        public DateTime FechaComentario { get; set; }
        public int IdPost { get; set; }
    }
}
