﻿using Microsoft.EntityFrameworkCore;
using T1_EXAMEN.Models.Maps;

namespace T1_EXAMEN.Models
{
    public class T1_EXAMEN_CONTEXT : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public T1_EXAMEN_CONTEXT(DbContextOptions<T1_EXAMEN_CONTEXT> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PostMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());
        }
    }
}
