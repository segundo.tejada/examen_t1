﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace T1_EXAMEN.Models.Maps
{
    public class PostMap : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Comentario).
                WithOne().
                HasForeignKey(o => o.IdPost);
        }
    }
}
